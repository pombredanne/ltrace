// TODO
#include <sys/ptrace.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <stdint.h>

#include <utils/breakpoints.h>
#include <utils/elfinfo.h>
#include <utils/tools.h>
#include <utils/conf.h>

#include <my_hooker/setup.h>


static void
_do_ld_linking(pid_t child, struct elf* elf, uint32_t* status)
{
    // drop breakpoint and continue
    struct bp main = { .plt_addr = elf->ehdr->e_entry };
    bp_insert(child, &main);
    ptrace(PTRACE_CONT, child, NULL, NULL);
    waitpid(child, (int32_t*) status, 0);

    // restore instruction
    struct user_regs_struct regs;
    ptrace(PTRACE_GETREGS, child, NULL, &regs); regs.rip -= 1;
    ptrace(PTRACE_SETREGS, child, NULL, &regs);
    ptrace(PTRACE_POKEDATA, child, main.plt_addr, main.instr);
}


int main(int argc, char* argv[])
{
    uint32_t bps_count = 0, sharedmem_fd = 0, status = 0, ret = 0;
    struct elf* elf = NULL;
    struct bp* bps = NULL;
    pid_t child;

    if (argc < 2)
        return 2;
    else if (setenv("LD_BIND_NOW", "1", 1) != 0)
        return 3;
    else if ((elf = elf_init(argv[1])) == NULL)
        return 4;
    else if ((bps = bp_get(elf, &bps_count, 1)) == NULL)
        return 5;
    else if ((sharedmem_fd = setup_tmp_file(bps, bps_count)) == -1)
        return 6;
    else if ((child = fork()) == 0)
    {
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        if (fcntl(sharedmem_fd, F_SETFD, !FD_CLOEXEC) != 0)
            return 2;
        execvp(argv[1], argv + 1);
    }
    else if (child != -1)
    {
        // wait for the child to start
        waitpid(child, (int32_t*) &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 2)
            return 7;

        // go to main (dynamic linking done by then)
        _do_ld_linking(child, elf, &status);

        // perform mmap in the callee
        void* distshared = setup_distant(child, bps, sharedmem_fd, &status);

        // setup the stubs in the child's plt
        setup_stubs(child, distshared, bps, bps_count);

        // drop the child process
        ptrace(PTRACE_DETACH, child, NULL, NULL);
    }
    else
        ret = 8;

    if (ret != 0 || errno != 0)
        printf("fatal error: %s\n", strerror(errno));

    // free and leave
    bp_free(bps, bps_count, 1);
    elf_destroy(elf);
    return errno;
}
