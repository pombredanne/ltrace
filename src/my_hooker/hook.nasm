BITS 64


_start:
    jmp     main

data:
    dq      0x0 ; call identifier
    dq      0x0 ; call address


main:
    ; setup the stack correctly and store the library call identifier
    mov     rax, QWORD [rsp]
    mov     [rel data], rax
    mov     rax, QWORD [rsp + 8]
    add     rsp, 16
    push    rax
    push    rcx

    ; process offset of library call in array
    mov     rax, [rel data]
    mov     rdx, 4 * 0x8
    mul     rdx
    lea     rdx, [rel bps_arr]
    add     rax, rdx

    push    rax
    call    strlen
    call    print_libcall
    pop     rax

    ; prepare jump address
    mov     rax, QWORD [rax + 0x10]
    mov     rax, QWORD [rax]
    mov     QWORD [rel data + 0x8], rax

    ; restore registers before jump. departure in 5, 4, 3...
    pop     rdx
    pop     rax

    ; ... 2, 1...
    jmp     QWORD [rel data + 0x8]


; string address in rax, return value in rdx. doesnt trash anything else
strlen:
    push    rcx
    mov     rdx, QWORD [rax]

    strlen_loop:
    mov     cl, BYTE [rdx]
    inc     rdx
    cmp     cl, 0
    jne     strlen_loop
    sub     rdx, QWORD [rax]
    pop     rcx
    ret


; doesnt trash anything, rax is string address and rdx string length
print_libcall:
    push    rdi
    push    rsi

    mov     rdi, 1
    mov     rsi, QWORD [rax]
    mov     rax, 1
    syscall

    pop     rsi
    pop     rdi
    ret

ALIGN 0x4
bps_arr:
