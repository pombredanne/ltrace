#include <my_hooker/setup.h>

#define PROT_FLAGS (PROT_READ | PROT_WRITE | PROT_EXEC)
#define MAP_FLAGS (MAP_SHARED)
#define MAP_SIZE (1024)


static uint32_t
_load_code(const char* codefile, char* shared)
{
    //TODO: change address of stub
    uint32_t size;
    int fd = open(codefile, O_RDONLY);
    if (fd == -1)
        return 0;
    size = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);

    // load code in the shared zone
    if (read(fd, shared, size) == -1)
        return 0;

    close(fd);
    return size;
}


uint32_t
setup_tmp_file(struct bp* bps, uint32_t bps_count)
{
    int fd = shm_open("/trace_shared", O_CREAT | O_RDWR, 0760);
    if (fd == -1)
        return -1;

    // TODO: extend file size
    const char init[MAP_SIZE] = { 0 };
    write(fd, init, MAP_SIZE);

    char* shared = mmap(NULL, MAP_SIZE, PROT_FLAGS, MAP_FLAGS, fd, 0);
    if (shared == MAP_FAILED)
        return -1;

    // copy main code
    uint32_t offset = _load_code("src/my_hooker/hook.bin", shared);
    if (offset == 0)
        return -1;

    // copy breakpoints
    memcpy(shared + offset, bps, bps_count * sizeof (struct bp));
    return (uint32_t) fd;
}


void*
setup_distant(pid_t child, struct bp* bp, uint32_t fd, uint32_t* status)
{
    struct user_regs_struct init_regs;
    struct user_regs_struct call_regs;

    ptrace(PTRACE_GETREGS, child, NULL, &init_regs);
    memcpy(&call_regs, &init_regs, sizeof (struct user_regs_struct));

    // setup temporary instruction at main plt stub
    uint64_t orig = ptrace(PTRACE_PEEKDATA, child, bp->plt_addr - 0x10, NULL);
    uint64_t new = (orig & 0xFFFFFFFFFF000000) | 0xcc050f;
    ptrace(PTRACE_POKEDATA, child, bp->plt_addr - 0x10, new);

    // setup registers previous to call (old ones are saved in init_regs)
    call_regs.rip = bp->plt_addr - 0x10; call_regs.rax = 9; call_regs.rdi = 0;
    call_regs.rsi = MAP_SIZE; call_regs.rdx = PROT_FLAGS;
    call_regs.r10 = MAP_FLAGS; call_regs.r8 = fd; call_regs.r9 = 0;
    ptrace(PTRACE_SETREGS, child, NULL, &call_regs);

    // execute syscall
    ptrace(PTRACE_CONT, child, NULL, NULL);
    waitpid(child, (int32_t*) status, 0);

    // retrieve eax syscall resyult and setup program for normal execution
    ptrace(PTRACE_GETREGS, child, NULL, &call_regs);
    ptrace(PTRACE_SETREGS, child, NULL, &init_regs);

    return (void*) call_regs.rax;
}


uint32_t
setup_child(const char* filename)
{
    return shm_open(filename, O_RDWR, 0760);
}


uint8_t
setup_stubs(pid_t child, void* chunk, struct bp* bps, uint32_t bps_count)
{
    // push rax then nopsled until the pushq
    static char* instr = "\x50\x90\x90\x90\x90\x90\x00\x00";

    for (uint32_t i = 0; i < bps_count; i += 1)
    {
        uint64_t curinstr = ptrace(PTRACE_PEEKDATA, child, bps[i].plt_addr);
        curinstr &= 0xFFFF000000000000; // keep the beginning of the pushq inst
        curinstr |= *(uint64_t*)instr & 0x0000FFFFFFFFFFFF;
        ptrace(PTRACE_POKEDATA, child, bps[i].plt_addr, curinstr);
    }

    uint64_t mov1 = ((uint64_t) chunk << 16) | 0xb848;
    ptrace(PTRACE_POKEDATA, child, bps->plt_addr - 16, mov1);
    uint64_t mov2_jmp = ((uint64_t) chunk >> 48) | (0x90909090e0ff << 16);
    ptrace(PTRACE_POKEDATA, child, bps->plt_addr - 8, mov2_jmp);
    return 0;
}
