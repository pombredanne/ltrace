#ifndef BREAKPOINTS
# define BREAKPOINTS

# include <stdlib.h>
# include <stdint.h>
# include <sys/ptrace.h>

# include <utils/tools.h>
# include <utils/elfinfo.h>


struct bp
{
    char* symbol_name;
    uint64_t plt_addr;
    uint64_t got_addr;
    uint64_t instr;
};


struct bp*
bp_find(void* cur_addr, struct bp* bps, uint32_t bps_count);

void
bp_insert(pid_t process, struct bp* bp);

struct bp*
bp_get(struct elf* elf, uint32_t* count, bool dist);

void
bp_free(struct bp* bps, uint32_t count, bool dist);


#endif // !BREAKPOINTS
