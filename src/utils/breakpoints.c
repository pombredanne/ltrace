#include <utils/breakpoints.h>


struct bp*
bp_find(void* cur_addr, struct bp* bps, uint32_t bps_count)
{
    for (uint32_t i = 0; i < bps_count; i += 1)
        if (bps[i].plt_addr == (uint64_t) cur_addr)
            return bps + i;
    return NULL;
}


void
bp_insert(pid_t process, struct bp* bp)
{
    bp->instr = peek_word(process, bp->plt_addr);
    uint64_t new_instr = (bp->instr & 0xFFFFFFFFFFFFFF00) | 0xCC;
    ptrace(PTRACE_POKEDATA, process, bp->plt_addr, new_instr);
}


struct bp*
bp_get(struct elf* elf, uint32_t* count, bool dist)
{
    // load and count relocations and load plt
    struct section rela = elf_get_section(elf, ".rela.plt", 0);
    struct section plt = elf_get_section(elf, ".plt", 0);
    Elf64_Rela* relas = (void*) rela.content;
    *count = rela.header->sh_size / rela.header->sh_entsize;

    // initialize breakpoints array
    struct bp* bps = malloc(*count * sizeof (struct bp));
    if (bps == NULL)
        return NULL;

    for (uint32_t i = 0; i < *count; i += 1)
    {
        char* sym = elf_get_dynsymbol(elf, ELF64_R_SYM(relas[i].r_info), dist);
        uint64_t stub = plt.header->sh_addr + (i + 1) * plt.header->sh_entsize;
        bps[i].symbol_name = sym + (dist ? elf->dynstr.header->sh_addr : 0);
        bps[i].plt_addr = stub;
        bps[i].got_addr = relas[i].r_offset;
    }

    // free relocations and plt sections, return breakpoints pointer
    elf_free_section(plt);
    elf_free_section(rela);
    return bps;
}


void
bp_free(struct bp* bps, uint32_t count, bool dist)
{
    if (bps == NULL)
        return;

    for (uint32_t i = 0; !dist && i < count; i += 1)
        elf_free_dynsymbol(bps[i].symbol_name);
    free(bps);
}
