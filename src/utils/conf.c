#include <utils/conf.h>

#define BUFFER_MAX 1024
#define COMMENT_CHAR ';'
#define IS_SYSCALL(str) (str[0] == 'S' && str[1] == 'Y' && str[2] == 'S')
#define VALID(chr) ((chr >= 'a' && chr <= 'z') ||\
                    (chr >= 'A' && chr <= 'Z') ||\
                    (chr >= '0' && chr <= '9') || (chr == '_'))


static struct
{
    const char* str;
    const char* format;
} _types[] =
{
    {"int", "%d"}, {"addr", "%p"}, {"void", "void"}, {"string", "\"%s\""},
    {"size_t", "%llu"}, {"long", "%ld"}, {"uint", "%u"}, {"ulong", "%lu"},
    {"octal", "%o"}, {"char", "'%c'"}, {"pid_t", "%d"}, {"gid_t", "%d"},
    {"uid_t", "%d"}, {"off_t", "%lld"}, {"mode_t", "%o"}, {"file", "%p"},
    {"short", "%hd"}, {"ushort", "%hu"}
};


const char*
_conf_get_format(const char* type_name)
{
    for (uint8_t i = 0; i < 18; i += 1)
        if (strcmp(type_name, _types[i].str) == 0)
            return _types[i].format;

    // if the type is not found, simply print it as an int
    printf("warning: %s: type not found (defaulted to int hexa)\n", type_name);
    return _types[1].format;
}


char**
_conf_split_line(char *buff)
{
    // on average, there are 3 tokens: return type, symbol and 1 argument
    char** tokens = malloc(4 * sizeof (const char*));
    for (uint8_t i = 0; *buff != ';' && *buff != ')'; i += 1, buff += 1)
    {
        // remove white space padding and setup beginning of token
        while (!VALID(*buff))
            buff += 1;
        tokens[i] = buff;

        // look for separator
        while (VALID(*buff))
            buff += 1;
        *buff = '\0';

        if (i >= 3)
            tokens = realloc(tokens, (i + 2) * sizeof (const char*));
        tokens[i + 1] = NULL;
    }

    return tokens;
}


void
_handle_symbol(struct fct* fct, char* symbol)
{
    if (!IS_SYSCALL(symbol))
    {
        fct->symbol = mlt_strdup(symbol);
        fct->syscall_ident = 0;
        return;
    }

    // jump over the SYS_ string part and isolate the syscall identifier
    char* ptr = symbol + 4;
    while (*ptr >= '0' && *ptr <= '9')
        ptr++;
    *ptr = '\0';

    // recover both the syscall identifier and the symbol
    fct->syscall_ident = atoi(symbol + 4);
    fct->symbol = mlt_strdup(ptr + 1);
}


static struct fct*
_conf_parse_line(char* buffer)
{
    char** tokens = _conf_split_line(buffer);
    struct fct* fct = malloc(sizeof (struct fct));

    // fill up the return value. it's the first argument in the list
    fct->args = malloc(sizeof (struct arg));
    fct->args->format = _conf_get_format(tokens[0]);
    fct->args->next = NULL;

    // syscalls have a special symbol prefix to identify their ID
    _handle_symbol(fct, tokens[1]);

    // initialize and fill out real arguments linked list
    struct arg* tail = fct->args;
    for (uint8_t i = 2; tokens[i] != NULL; i += 1)
    {
        struct arg* cur = malloc(sizeof (struct arg));
        tail->next = cur;
        cur->next = NULL;
        cur->format = _conf_get_format(tokens[i]);
        tail = cur;
    }

    fct->next = NULL;
    free(tokens);
    return fct;
}


static void
_conf_free_args(struct arg* arg)
{
    if (arg->next != NULL)
        _conf_free_args(arg->next);
    free(arg);
}


void
conf_free_fct(struct fct* fct)
{
    if (fct->symbol != NULL)
        free((char*) fct->symbol);
    if (fct->args != NULL)
        _conf_free_args(fct->args);
    if (fct->next != NULL)
        conf_free_fct(fct->next);
    free(fct);
}


struct fct*
conf_parse_file(const char* filename)
{
    FILE* file = NULL;
    if ((file = fopen(filename, "r")) == NULL)
        return NULL;

    struct fct* head, *cur_fct = NULL;
    char buffer[BUFFER_MAX] = { 0 };

    while (feof(file) == 0)
    {
        if (fgets(buffer, BUFFER_MAX, file) == NULL ||
            *buffer == COMMENT_CHAR || *buffer == '\n' || *buffer == '\0')
            continue;

        if (cur_fct != NULL)
        {
            cur_fct->next = _conf_parse_line(buffer);
            cur_fct = cur_fct->next;
        }
        else
            head = cur_fct = _conf_parse_line(buffer);
    }

    fclose(file);
    return head;
}


struct fct*
conf_find_function(struct fct* prots, const char* symbol)
{
    for (; prots != NULL; prots = prots->next)
        if (strcmp(symbol, prots->symbol) == 0)
            return prots;
    return NULL;
}


struct fct*
conf_find_syscall(struct fct* prots, uint8_t syscall_ident)
{
    for (; prots != NULL; prots = prots->next)
        if (syscall_ident == prots->syscall_ident)
            return prots;
    return NULL;
}
