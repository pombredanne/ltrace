#ifndef TOOLS
# define TOOLS

# include <stdlib.h>
# include <stdint.h>
# include <string.h>
# include <stdbool.h>
# include <sys/ptrace.h>


char*
mlt_strdup(const char* str);

uint64_t*
peek(uint32_t child, uint64_t* dist, uint64_t amnt);

uint64_t
peek_word(uint32_t child, uint64_t dist);


#endif // !TOOLS
