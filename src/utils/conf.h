#ifndef CONF
# define CONF

# include <stdbool.h>
# include <stdlib.h>
# include <stdio.h>

# include <utils/tools.h>


struct arg
{
    const char* format;
    struct arg* next;
};


struct fct
{
    // symbol is the name of the function, the first arg is the return value
    // syscall_ident is 0 if this is not a syscall
    const char* symbol;
    struct arg* args;
    uint8_t syscall_ident;

    struct fct* next;
};


// parse each line of the file and return the function matching the prototype
// and return all function objects as a linked list of struct fct
struct fct*
conf_parse_file(const char* filename);

// free the linked list allocated by conf_parse_file
void
conf_free_fct(struct fct* fct);

// finds the prototype of the given symbol in the given list of prototypes.
// returns null if not found
struct fct*
conf_find_function(struct fct* prots, const char* symbol);

// finds the prototype of the given syscall in the given list of prototypes.
// returns null if not found
struct fct*
conf_find_syscall(struct fct* prots, uint8_t syscall_ident);


#endif // !CONF
