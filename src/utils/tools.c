#include <utils/tools.h>
#define BSIZE 64


char*
mlt_strdup(const char* str)
{
    // strdup is not posix, redefinition here
    uint32_t n = strlen(str) + 1;
    char* dup = malloc(n);

    if(dup)
        strcpy(dup, str);
    return dup;
}


static bool
_peek_check_eos(uint64_t chunk)
{
    for (uint8_t i = 0; i < sizeof (uint64_t); i += 1)
    {
        if ((chunk & 0xFF) == 0)
            return true;
        chunk = chunk >> 8;
    }

    return false;
}


uint64_t*
peek(uint32_t child, uint64_t* dist, uint64_t amnt)
{
    uint64_t* loc = malloc((amnt == 0 ? BSIZE : amnt) * sizeof (uint64_t));
    uint64_t lim = (amnt == 0 ? BSIZE : amnt);
    uint64_t amount = (amnt == 0 ? (uint64_t) -1 : amnt);

    for (uint32_t i = 0; i < amount; i++, dist++)
    {
        if (i == lim)
            loc = realloc(loc,
                (lim += (amnt == 0 ? BSIZE : amnt)) * sizeof (uint64_t));

        loc[i] = ptrace(PTRACE_PEEKDATA, child, dist, NULL);
        if (amnt == 0 && _peek_check_eos(loc[i]))
            break;
    }

    return loc;
}


uint64_t
peek_word(uint32_t child, uint64_t dist)
{
    return ptrace(PTRACE_PEEKDATA, child, (void *) dist, NULL);
}
