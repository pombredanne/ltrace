#include <my_ltrace/display.h>


static struct fct* proto = NULL;


void
_display_argument(pid_t child, struct arg* arg, uint64_t value, bool comma)
{
    uint8_t intact = 1;
    const char* format = arg->format;

    if (strcmp(arg->format, "\"%s\"") == 0)
    {
        if ((uint64_t*) value != NULL)
            value = (uint64_t) peek(child, (uint64_t*) value, intact = 0);
        else
            format = "%p";
    }

    printf(format, value);

    if (comma)
        printf(", ");
    if (!intact)
        free((uint64_t*) value);
}


void
display_proto(pid_t child, struct user_regs_struct* regs, const char* sym,
    struct fct* prototypes)
{
    printf("%s(", sym);

    if ((proto = conf_find_function(prototypes, sym)) != NULL)
    {
        uint64_t values[] = { regs->rdi, regs->rsi, regs->rdx, regs->rcx,
            regs->r8, regs->r9 };

        struct arg* as = proto->args->next;
        for (uint8_t i = 0; as != NULL; as = as->next, i += 1)
            _display_argument(child, as, values[i], !(as->next == NULL));
    }

    printf(") = ");
    fflush(stdout);
}


void
display_ret(pid_t child, struct user_regs_struct* regs)
{
    if (proto != NULL)
        _display_argument(child, proto->args, regs->rax, false);
    else
        printf("%ld", (int64_t) regs->rdi);
    printf("\n");
}
