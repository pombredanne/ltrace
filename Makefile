CFLAGS			= -Wall -Werror -pedantic -std=gnu99 -fPIC -g -ggdb3
INC				= -Isrc/
CC				= clang

UTILS_FIL		= elfinfo.c conf.c breakpoints.c tools.c
UTILS_SRC		= $(addprefix src/utils/, $(UTILS_FIL))
UTILS_OBJ		= $(UTILS_SRC:.c=.o)
UTILS_LIB		= $(addprefix src/utils/, libutils.so)

MY_STRACE_FIL	= main.c
MY_STRACE_SRC	= $(addprefix src/my_strace/, $(MY_STRACE_FIL))
MY_STRACE_OBJ	= $(MY_STRACE_SRC:.c=.o)
MY_STRACE_BIN	= my_strace
MY_STRACE_DEP	= $(UTILS_LIB)

MY_LTRACE_FIL	= display.c main.c
MY_LTRACE_SRC	= $(addprefix src/my_ltrace/, $(MY_LTRACE_FIL))
MY_LTRACE_OBJ	= $(MY_LTRACE_SRC:.c=.o)
MY_LTRACE_BIN	= my_ltrace
MY_LTRACE_DEP	= $(UTILS_LIB)

MY_HOOKER_FIL	= main.c setup.c
MY_HOOKER_SRC	= $(addprefix src/my_hooker/, $(MY_HOOKER_FIL))
MY_HOOKER_OBJ	= $(MY_HOOKER_SRC:.c=.o)
MY_HOOKER_BIN	= my_hooker
MY_HOOKER_DEP	= $(UTILS_LIB)


all: $(MY_HOOKER_BIN) $(MY_LTRACE_BIN)


$(UTILS_LIB): $(UTILS_OBJ)
	$(CC) $(CFLAGS) $(INC) -shared $(UTILS_OBJ) -o $@


$(MY_LTRACE_BIN): $(MY_LTRACE_DEP) $(MY_LTRACE_OBJ)
	$(CC) $(CFLAGS) $(INC) $(MY_LTRACE_DEP) $(MY_LTRACE_OBJ) -o $@ -W,-rpath,$(MY_LTRACE_DEP)


$(MY_STRACE_BIN): $(MY_STRACE_DEP) $(MY_STRACE_OBJ)
	$(CC) $(CFLAGS) $(INC) $(MY_STRACE_DEP) $(MY_STRACE_OBJ) -o $@ -W,-rpath,$(MY_STRACE_DEP)


$(MY_HOOKER_BIN): $(MY_HOOKER_DEP) $(MY_HOOKER_OBJ)
	$(CC) $(CFLAGS) $(INC) $(MY_HOOKER_DEP) $(MY_HOOKER_OBJ) -lrt -o $@ -W,-rpath,$(MY_HOOKER_DEP)
	nasm src/my_hooker/hook.nasm -o src/my_hooker/hook.bin


%.o: %.c
	$(CC) $(CFLAGS) $(INC) -c $< -o $@
clean:
	rm -rf $(MY_LTRACE_OBJ) $(MY_STRACE_OBJ) $(MY_HOOKER_OBJ) $(UTILS_OBJ)
distclean: clean
	rm -rf $(MY_LTRACE_BIN) $(MY_STRACE_BIN) $(MY_HOOKER_BIN) $(UTILS_LIB) src/my_hooker/hook.bin
